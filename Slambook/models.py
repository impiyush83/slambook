from sqlalchemy import String, Column, ForeignKey, Integer, Boolean
from sqlalchemy.orm import relationship
from sqlalchemy_wrapper import SQLAlchemy

db = SQLAlchemy('sqlite:///slambook.db')


class Parent(db.Model):
    __tablename__ = "parent"

    fn = Column(String)
    sn = Column(String)
    email = Column(String, primary_key=True)
    password = Column(String)
    gender = Column(String)
    is_login = Column(Boolean, default=False)
    childs = relationship('Child')


class Child(db.Model):
    __tablename__ = "child"

    id = Column(Integer, primary_key=True)
    email = Column(String, ForeignKey('parent.email'))
    friend = relationship("Parent")
    fn = Column(String)
    sn = Column(String)
    gender = Column(String)
    fcolor = Column(String)
    ffood = Column(String)
    fsong = Column(String)
    tel = Column(Integer)


class Secret(db.Model):
    __tablename__ = "secret"

    email = Column(String, ForeignKey('parent.email'), unique=True)
    secret_key = Column(String, primary_key=True)


db.create_all()
