from flask import Flask, render_template, request, redirect, url_for, session, g
from models import Parent, db, Child, Secret
import os

app = Flask(__name__)

app.secret_key = os.urandom(24)


@app.route('/enter_secret_key', methods=['POST', 'GET'])
def enter_secret_key():
    if g.profile:
        try:
            return render_template('enter_secret_key.html')
        except:
            return redirect(url_for('logged_in'))
    else:
        return redirect(url_for('index'))


@app.route('/create_secret_key', methods=['POST', 'GET'])
def create_secret_key():
    print g.profile
    if g.profile:
        try:
            import random
            import string
            random = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(32)])
            # print random
            obj = db.query(Parent).filter(Parent.email == g.profile).first()
            # print obj.email
            db.add(Secret(email=obj.email, secret_key=random))
            db.commit()
            # print "2"
            return render_template('copy_secret_key.html', random=random)
        except:
            return redirect(url_for('logged_in'))
    else:
        return redirect(url_for('index'))


@app.route('/check_secret_key', methods=['POST', 'GET'])
def check_secret_key():
    if g.profile:
        try:
            secret_key = request.form['secret_key']
            owner_of_secret_key = db.query(Secret).filter(Secret.secret_key == secret_key).all()
            if len(owner_of_secret_key) == 1:
                return render_template('remote_register.html', owner_of_secret_key=owner_of_secret_key[0].email)
            else:
                return redirect(url_for('logged_in'))
        except:
            return redirect(url_for('logged_in'))
    else:
        return redirect(url_for('index'))


@app.route('/remote_upload', methods=['POST'])
def remote_upload():
    if g.profile:
        try:
            fn = request.form['fname']
            sn = request.form['sname']
            fcolor = request.form['fcolor']
            ffood = request.form['ffood']
            fsong = request.form['fsong']
            gender = request.form['gender']
            tel = request.form['tel']
            secret_key_object = request.form['secret_key']

            if str(secret_key_object) != str(g.profile):
                obj_of_referer = db.query(Parent).filter(Parent.email == str(secret_key_object)).first()
                db.add(
                    Child(fn=fn, sn=sn, fcolor=fcolor, ffood=ffood, fsong=fsong, gender=gender, friend=obj_of_referer,
                          tel=tel))
                db.commit()
                db.delete(db.query(Secret).filter(Secret.email == secret_key_object).first())
                db.commit()
                return redirect(url_for('logged_in'))  # success page
            else:
                return redirect(url_for('logged_in'))
        except:
            return redirect(url_for('index'))
    else:
        return redirect(url_for('index'))


@app.route('/')
def index():
    if g.profile == None and g.is_login==False:
        return render_template('login_and_sign_up.html')
    else:
        return "Logged in from other server"


@app.route('/upload', methods=['POST'])
def upload():
    if g.profile:
        try:
            fn = request.form['fname']
            sn = request.form['sname']
            fcolor = request.form['fcolor']
            ffood = request.form['ffood']
            fsong = request.form['fsong']
            gender = request.form['gender']
            tel = request.form['tel']
            obj = db.query(Parent).filter(Parent.email == g.profile).first()
            db.add(Child(fn=fn, sn=sn, fcolor=fcolor, ffood=ffood, fsong=fsong, gender=gender, friend=obj, tel=tel))
            db.commit()
            return redirect(url_for('logged_in'))
        except:
            return redirect(url_for('add_friend'))
    else:
        return redirect(url_for('index'))


@app.route('/getsession')
def getsession():
    if 'profile' in session:
        return session['profile']
    return "Not logged in"


@app.route('/dropsession')
def dropsession():
    session.pop('profile', None)
    g.profile = None
    return 'Dropped'


@app.before_request
def before_request():
    g.profile = None
    parent_to_be_logged_out = db.query(Parent).filter(Parent.email == str(session['profile']).strip()).first()
    if 'profile' in session and parent_to_be_logged_out.is_login:
        g.profile = session['profile']


@app.route('/logout')
def logout():
    parent_to_be_logged_out = db.query(Parent).filter(Parent.email == str(session['profile']).strip()).first()
    parent_to_be_logged_out.is_login = False
    session.clear()
    return redirect(url_for('index'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    try:
        if request.method == "POST":
            print "post"
            email = request.form['email']
            password = request.form['password']
            objects = db.query(Parent).filter().all()
            flag = 0
            for i in objects:
                if str(i.email).strip() == str(email).strip() and str(i.password).strip() == str(password).strip():
                    flag = 1
                    print "2"
                    break
            if flag == 1:
                parent = db.query(Parent).filter(Parent.email == str(email).strip()).first()
                if not parent.is_login:
                    session['profile'] = email
                    print "1"
                    return redirect(url_for('logged_in'))
                else:
                    return "Logged in from other server"
            else:
                return redirect(url_for('index'))
    except:
        print "3"
        return redirect(url_for('index'))


@app.route('/addfriend', methods=['POST', 'GET'])
def add_friend():
    if g.profile:
        return render_template('registration.html')
    else:
        return redirect(url_for('index'))


@app.route('/home')
def logged_in():
    try:
        print "5"
        if session['profile']:
            print "6"
            friends = db.query(Child).filter(Child.email == g.profile).all()
            len1 = len(friends)
            print "7"
            object_to_check_secret_key_created = db.query(Secret).filter(Secret.email == g.profile).all()
            print "8"
            lenx = len(object_to_check_secret_key_created)
            print "9"
            return render_template('homepage.html', friends=friends, len=len1, lenx=lenx)

        return redirect(url_for('index'))
    except:
        return redirect(url_for('index'))


@app.route('/insertuser', methods=['POST', 'GET'])
def insertuser():
    try:
        fn = request.form['fn']
        sn = request.form['sn']
        email = request.form['email']
        password = request.form['pass']
        gender = request.form['gender']
        u1 = Parent(fn=fn, sn=sn, email=email, password=password, gender=gender)
        db.add(u1)
        db.commit()
        return redirect(url_for('index'))
    except:
        return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True, port=3859)
